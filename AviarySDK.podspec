
Pod::Spec.new do |s|

  s.name         = "AviarySDK"
  s.version      = "0.0.5"
  s.summary      = "CreativeSDK is a HUGE replacement for tiny AviarySDK"

  s.homepage     = "https://bitbucket.org/develop2k/aviarysdk/overview"
  s.author       =  'Adobe'
  s.license      = { :type => 'MIT' }

  s.source              = { :git => "https://develop2k@bitbucket.org/develop2k/aviarysdk.git", :tag => s.version }
  s.preserve_paths      = 'AdobeCreativeSDKImage.framework', 'AdobeCreativeSDKCore.framework'
  s.resources           = 'AdobeCreativeSDKImage.framework/Versions/A/Resources/AdobeCreativeSDKImageResources.bundle', 'AdobeCreativeSDKCore.framework/Versions/A/Resources/AdobeCreativeSDKCoreResources.bundle'
  s.vendored_frameworks = 'AdobeCreativeSDKImage.framework', 'AdobeCreativeSDKCore.framework'

  s.frameworks = [
    'SystemConfiguration',
    'CoreData',
    'Accelerate',
    'MessageUI',
    'OpenGLES',
    'QuartzCore',
    'StoreKit',
    'MobileCoreServices'
  ]

  s.libraries = [
    'z',
    'sqlite3',
    'c++'
  ]

  s.requires_arc = true
  
  s.xcconfig = { 'USE_CSDK_COMPONENTS' => '1' }

end
